package com.github.ahorcadoarcade;

public interface OponentInterface {

    void sendMistake ();
    void sendSuccess (String solution);

}
