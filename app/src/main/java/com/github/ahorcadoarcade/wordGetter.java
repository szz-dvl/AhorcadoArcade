package com.github.ahorcadoarcade;

import android.os.AsyncTask;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;


public class wordGetter extends AsyncTask<Character, Void, String> {

    WordGetter context;
    String lang;

    public wordGetter (WordGetter context, String lang) {

        this.context = context;
        this.lang = lang;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Character... chars) {

        String query = "http://api.datamuse.com/words?sp=" + chars[0] + "*&" + (this.lang.equals("es") ? "v=es" : "");

        try {

            URL url = new URL(query);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responsecode = conn.getResponseCode();

            if(responsecode != 200)

                throw new RuntimeException("HttpResponseCode: " +responsecode);

            else
            {

                BufferedReader entrada = new BufferedReader(new InputStreamReader(url.openStream()));
                String chunk = "";
                String NL = System.getProperty("line.separator");
                StringBuffer res = new StringBuffer("");

                while ((chunk = entrada.readLine()) != null) {
                    res.append(chunk + NL);
                }

                entrada.close();
                return res.toString();
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onPostExecute(String words) {

        if (words.equals(""))

            this.context.onWordSelected(null);

        else {

            try {

                JSONParser parse = new JSONParser();
                JSONArray array = (JSONArray) parse.parse(words);

                Random rnd = new Random();

                int count = rnd.nextInt(10);

                while (--count > 0)
                    rnd.nextInt(array.size());

                JSONObject obj = (JSONObject) array.get(rnd.nextInt(array.size()));
                String word = (String) obj.get("word");

                while (word.length() < 6) { /* Level */

                    count = rnd.nextInt(10);

                    while (--count > 0)
                        rnd.nextInt(array.size());

                    obj = (JSONObject) array.get(rnd.nextInt(array.size()));
                    word = (String) obj.get("word");

                }

                this.context.onWordSelected(word);

            } catch(Exception ex) {

                ex.printStackTrace();
                this.context.onWordSelected(null);

            }
        }
    }
}
