package com.github.ahorcadoarcade;

import android.app.Activity;

import com.github.nkzawa.emitter.Emitter;

public class EventListener implements Emitter.Listener {

    MessageGetter context;

    public EventListener(MessageGetter context) {

        this.context = context;

    }

    @Override
    public void call(final Object... args) {

        context.onMessageReceived(args);

    }
}
