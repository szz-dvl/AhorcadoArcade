package com.github.ahorcadoarcade;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.URISyntaxException;

public class NotifyReceiver extends BroadcastReceiver {

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        try {

            SharedPreferences sharedPref = context.getSharedPreferences(
                    context.getString(R.string.pref_file), Context.MODE_PRIVATE);

            String guid = sharedPref.getString(context.getString(R.string.guid), "NO_GUID");


            mSocket.emit("challenge_rejected",
                    guid,
                    intent.getStringExtra("com.github.ahorcadoarcade.PHONE"),
                    intent.getIntExtra("com.github.ahorcadoarcade.ID", 0)
            );


        } catch(Exception ex) {
            ex.printStackTrace();
        }
        //This is used to close the notification tray
        //Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        //context.sendBroadcast(it);
    }

}
