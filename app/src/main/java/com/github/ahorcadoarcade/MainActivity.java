package com.github.ahorcadoarcade;


import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.media.AudioAttributes;

import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.socketio.client.IO;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements MessageGetter{

    private Boolean created = false;
    private Boolean has_phone = false;
    private String guid;
    private String lang;
    private String phone;
    private int current_id = 0;

    final static int MY_PERMISSIONS_REQUEST_SEND_SMS = 1;
    final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 2;

    private Boolean HAVE_CONTACTS = false;
    private Boolean HAVE_SMS = false;

    private List<ChallengeNotification> notis = new ArrayList<>();

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            this.created = savedInstanceState.getBoolean("MAIN_DONE");
            this.has_phone = savedInstanceState.getBoolean("HAS_PHONE");
            this.guid = savedInstanceState.getString("GUID");
            this.lang = savedInstanceState.getString("LANG");
            this.phone = savedInstanceState.getString("PHONE");
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS},
                    MY_PERMISSIONS_REQUEST_SEND_SMS);
        } else
            HAVE_SMS = true;

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            HAVE_CONTACTS = false;

        } else
            HAVE_CONTACTS = true;

        mSocket.on("main", new EventListener(this));

        if (!this.created) {

            SharedPreferences sharedPref = this.getSharedPreferences(
                    getString(R.string.pref_file), Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.def_language),
                    getResources().getConfiguration().locale.toString().substring(0, 2));
            editor.apply();

            guid = sharedPref.getString(getString(R.string.guid), "NO_GUID");

            if (guid.equals("NO_GUID")) {

                guid = UUID.randomUUID().toString();
                editor.putString(getString(R.string.guid), guid);
                editor.apply();

            }

            String defaultLang = sharedPref.getString(getString(R.string.def_language),
                        getResources().getString(R.string.def_lang));

            lang = sharedPref.getString(getString(R.string.language), defaultLang);
            phone = sharedPref.getString(getString(R.string.phone), "");

            if (phone.length() > 0)
                this.has_phone = true;

            mSocket.connect();

        }

        this.created = true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else
                    HAVE_CONTACTS = true;

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    HAVE_SMS = true;

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    HAVE_SMS = false;
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    HAVE_CONTACTS = true;

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    HAVE_CONTACTS = false;

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("MAIN_DONE", this.created);
        outState.putBoolean("HAS_PHONE", this.has_phone);
        outState.putString("GUID", this.guid);
        outState.putString("LANG", this.lang);
        outState.putString("PHONE", this.phone);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    private Boolean have_rights () { return HAVE_CONTACTS && HAVE_SMS; }

    public void playDaGame (View view) {

        if (has_phone)
            startActivity(new Intent(this, ModosJuego.class));
        else {

            if (have_rights()) {

                startActivity(new Intent(this, CheckPhoneSMS.class));
                finish();
                mSocket.off();

            } else
                startActivity(new Intent(this, getWord.class));
        }

    }

    public void configApp (View view) {

        Intent intent = new Intent(this, configApp.class);
        intent.putExtra("HAVE_SMS", HAVE_SMS);

        startActivity(intent);

    }


    @Override
    public void onMessageReceived(final Object... args) {

        System.out.println("Received main: ");

        try {

            JSONObject data = (JSONObject) args[0];

            System.out.println(data);

            String action = (String) data.get("action");

            switch (action) {
                case "who_are_you":
                    mSocket.emit("hello", guid, lang, has_phone ? phone : "NULL");
                    break;
                case "clean_noti": /* Necesario hello? */ {
                    String id = (String) data.get("id");

                    if (id.equals(guid)) {

                        int not = (Integer) data.get("noti_id");

                        for (ChallengeNotification nt : notis) {

                            if (nt.NOTIFY_ID == not) {

                                nt.cancelChallenge();
                                notis.remove(nt);

                            }

                        }
                    }
                }
                break;

                case "notify": /* Retado por alguien */

                    if (HAVE_CONTACTS) {

                        String id = (String) data.get("id");

                        if (id.equals(guid)) {

                            String phone = (String) data.get("phone");

                            notis.add(new ChallengeNotification(this, phone, current_id++));

                        }
                    }

                    break;
                default:
                    break;
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }
}
