package com.github.ahorcadoarcade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Counter {

    static Map<String, Long> puntuacion;

    public Counter () {

        puntuacion = new HashMap<String, Long>();
        puntuacion.put("a", (long) 0);
        //puntuacion.put("á", (long) 0);
        puntuacion.put("b", (long) 0);
        puntuacion.put("c", (long) 0);
        puntuacion.put("d", (long) 0);
        puntuacion.put("e", (long) 0);
        //puntuacion.put("é", (long) 0);
        puntuacion.put("f", (long) 0);
        puntuacion.put("g", (long) 0);
        puntuacion.put("h", (long) 0);
        puntuacion.put("i", (long) 0);
        //puntuacion.put("í", (long) 0);
        puntuacion.put("j", (long) 0);
        puntuacion.put("k", (long) 0);
        puntuacion.put("l", (long) 0);
        puntuacion.put("m", (long) 0);
        puntuacion.put("n", (long) 0);
        puntuacion.put("ñ", (long) 0);
        puntuacion.put("o", (long) 0);
        //puntuacion.put("ó", (long) 0);
        puntuacion.put("p", (long) 0);
        puntuacion.put("q", (long) 0);
        puntuacion.put("r", (long) 0);
        puntuacion.put("s", (long) 0);
        puntuacion.put("t", (long) 0);
        puntuacion.put("u", (long) 0);
        //puntuacion.put("ú", (long) 0);
        puntuacion.put("v", (long) 0);
        puntuacion.put("w", (long) 0);
        puntuacion.put("x", (long) 0);
        puntuacion.put("y", (long) 0);
        puntuacion.put("z", (long) 0);

    }

    public void addPoint (String key, long score) {

//        System.out.println("Point " + key);

        Long actual = this.puntuacion.get(key);

//        System.out.println("Actual " + actual);

        this.puntuacion.put(key, actual + score);

    }

    public String getMax() {

        Random rnd = new Random();

        List<String> maxims = getAllMax();
        System.out.println(maxims);

        int idx = rnd.nextInt(maxims.size());

        return maxims.get(idx);
    }

    private List<String> getAllMax () {

        long max = 0;
        List<String> set = new ArrayList<>();

        for (int i = 0; i < this.puntuacion.size(); i++) {

            String key = (String) this.puntuacion.keySet().toArray()[i];
            //System.out.println("Puntuacion: " + key + " " + this.puntuacion.get(key));

            if (this.puntuacion.get(key) > max)
                 max = this.puntuacion.get(key);

        }

        for (int i = 0; i < this.puntuacion.size(); i++) {

            //System.out.println("Puntuacion: " + (char) (i + 'a') + " " + this.puntuacion.get(i + 'a'));

            String key = (String) this.puntuacion.keySet().toArray()[i];

            if (this.puntuacion.get(key) == max)
                set.add(key);

        }

        return set;
    }

}
