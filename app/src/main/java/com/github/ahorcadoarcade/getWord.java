package com.github.ahorcadoarcade;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class getWord extends AppCompatActivity implements RaeGetter {

    Boolean allow = true;
    String lang;
    Boolean challenge;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {

            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getSupportActionBar().hide();
            setContentView(R.layout.activity_get_word);

            Intent intent = getIntent();
            String phone = intent.getStringExtra("com.github.ahorcadoarcade.PHONE");
            challenge = intent.getBooleanExtra("com.github.ahorcadoarcade.CHALLENGE", false);

            SharedPreferences sharedPref = getSharedPreferences(
                    getString(R.string.pref_file), Context.MODE_PRIVATE);

            String defaultLang = sharedPref.getString(getString(R.string.def_language), getResources().getString(R.string.def_lang));
            this.lang = sharedPref.getString(getString(R.string.language), defaultLang);

            if (phone != null) {

                String guid = sharedPref.getString(getString(R.string.guid), "NO_GUID");
                int noti_id = intent.getIntExtra("com.github.ahorcadoarcade.ID", 0);

                mSocket.emit("challenge_accepted", guid, phone, noti_id);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void badWord () {

        TextView tv = (TextView) this.findViewById(R.id.tip);
        tv.setText(getResources().getString(R.string.bad_word));
        this.allow = true;

        (new Thread() {
            @Override
            public void run() {
                try {

                    Thread.sleep(3000);
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            TextView tv = (TextView) findViewById(R.id.tip);
                            tv.setText(getResources().getString(R.string.choose_word));

                        }
                    });

                }  catch (Exception ex) {

                    ex.printStackTrace();

                }
            }
        }).start();

    }

    @Override
    public void onWordChecked (Boolean res) {

        if (res) {


            Intent intent = new Intent(this, PrepareMatch.class);
            EditText editText = (EditText) findViewById(R.id.palabra);
            String word = editText.getText().toString();
            intent.putExtra("com.github.ahorcadoarcade.SECRET", word);
            intent.putExtra("com.github.ahorcadoarcade.CHALLENGE", challenge);
            startActivity(intent);
            finish();

        } else {

            badWord();

//          IBinder Token = koimg.getWindowToken();
//          InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
//          imm.hideSoftInputFromWindow(Token, 0);
        }
    }

    public void checkText(View view) {

        if (this.allow) {

            EditText editText = (EditText) findViewById(R.id.palabra);
            String message = editText.getText().toString();

            if (message.matches("[a-z á é í ó ú ñ]+")) {

                new checkFromRAE(this, lang).execute(message);
                this.allow = false;

            } else
                badWord();


        }
    }

}
