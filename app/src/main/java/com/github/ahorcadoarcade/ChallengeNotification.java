package com.github.ahorcadoarcade;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class ChallengeNotification implements Timer {

    private int spent = 0;
    private Context ctx;
    private AsyncTask to;
    private NotificationCompat.Builder builder;
    private NotificationManager notifManager;
    private Boolean canceled;

    public int NOTIFY_ID;

    public ChallengeNotification (Context c, String phone, int id) {

        this.ctx = c;
        this.NOTIFY_ID = id;

        Intent accept = new Intent(this.ctx, getWord.class);
        accept.putExtra("com.github.ahorcadoarcade.CHALLENGE", true);
        accept.putExtra("com.github.ahorcadoarcade.PHONE", phone);
        accept.putExtra("com.github.ahorcadoarcade.ID", NOTIFY_ID);

        PendingIntent pi_accept = PendingIntent.getActivity(this.ctx, 1, accept, PendingIntent.FLAG_ONE_SHOT);

        Intent decline = new Intent(this.ctx, NotifyReceiver.class);
        decline.putExtra("com.github.ahorcadoarcade.ID", NOTIFY_ID);
        decline.putExtra("com.github.ahorcadoarcade.PHONE", phone);
        PendingIntent pi_decline = PendingIntent.getBroadcast(this.ctx, 1, decline, PendingIntent.FLAG_ONE_SHOT);

        notifManager = (NotificationManager) this.ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        String contact_id = getContactInfo(phone, ContactsContract.PhoneLookup._ID);

        builder = new NotificationCompat.Builder(this.ctx, "challenge")
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.somebody)
                .setContentText("Te reta a un ahorcado.")
                .setProgress(60, 0, false)
                .setPriority(Notification.PRIORITY_HIGH)
                .addAction(new NotificationCompat.Action(0, "Aceptar", pi_accept))
                .addAction(new NotificationCompat.Action(0, "Rechazar", pi_decline));

        if (contact_id != null) {

            Uri photo = getPhotoURI(contact_id);

            if (photo != null)
                builder.setLargeIcon(getImageBitmapFromURI(photo));
            else
                builder.setLargeIcon(drawableToIcon(this.ctx.getResources().getDrawable(R.drawable.somebody)));

            builder.setContentTitle(getContactInfo(phone, ContactsContract.PhoneLookup.DISPLAY_NAME));

        } else {

            builder.setContentTitle(phone);
            builder.setLargeIcon(drawableToIcon(this.ctx.getResources().getDrawable(R.drawable.somebody)));

        }

        notifManager.notify(NOTIFY_ID, builder.build());
        to = new timeOut(this).execute(1000);
    }

    public void cancelChallenge () {

        this.canceled = true;
        to.cancel(true);
        notifManager.cancel(NOTIFY_ID);

    }

    @Override
    public void onTimeout() {

        /* Start AI if server is down. */
        spent ++;

        if (spent > 60) {

            notifManager.cancel(NOTIFY_ID);

         } else {

                builder.setProgress(60, spent, false);
                builder.setDefaults(0);
                notifManager.notify(NOTIFY_ID, builder.build());

                if (!this.canceled)
                    to = new timeOut(this).execute(1000);
        }
    }

    /* @ https://stackoverflow.com/questions/3035692/how-to-convert-a-drawable-to-a-bitmap */
    private static Bitmap drawableToIcon (Drawable drawable) {

        Bitmap bitmap = Bitmap.createBitmap(75, 75, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /* @ https://stackoverflow.com/questions/3079365/android-retrieve-contact-name-from-phone-number */
    private String getContactInfo (final String phoneNumber, String val)
    {
        Uri uri = Uri.withAppendedPath(

                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber)
        );

        String[] projection = new String[]{ val };

        String contactNfo = null;

        Cursor cursor = this.ctx.getContentResolver().query(
                uri,
                projection,
                null,
                null,
                null
        );

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactNfo = cursor.getString(0);
            }
            cursor.close();
        }

        return contactNfo;
    }

    /* @ https://stackoverflow.com/questions/13863879/load-contact-image-into-bitmap */
    private Uri getPhotoURI(String contactId) {

        try {
            Cursor cursor = this.ctx.getContentResolver()
                    .query(ContactsContract.Data.CONTENT_URI,
                            null,
                            ContactsContract.Data.CONTACT_ID
                                    + "="
                                    + contactId
                                    + " AND "
                                    + ContactsContract.Data.MIMETYPE
                                    + "='"
                                    + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                    + "'", null, null);

            if (cursor != null) {
                if (!cursor.moveToFirst()) {
                    return null; // no photo
                }
            } else {
                return null; // error in cursor process
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Uri person = ContentUris.withAppendedId(
                ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId));
        return Uri.withAppendedPath(person,
                ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }

    private Bitmap getImageBitmapFromURI(Uri uri) {

        try {

            InputStream input = this.ctx.getContentResolver().openInputStream(uri);

            if (input == null)
                return null;

            return BitmapFactory.decodeStream(input);

        } catch(FileNotFoundException ex) {

            ex.printStackTrace();
            return null;
        }
    }
}
