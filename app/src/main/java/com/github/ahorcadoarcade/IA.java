package com.github.ahorcadoarcade;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IA implements OponentInterface {

    MatchActivity context;
    String status;
    List<String> tried = new ArrayList<>();
    List<String> solves = new ArrayList<>();
    String secret;
    String choosen;
    String word;
    int found;
    int current_level;
    String lang;

    @Override
    public void sendMistake() {
        this.context.opponentMistake();
    }

    @Override
    public void sendSuccess(String solution) {
        this.context.opponentSuccess(solution);
    }

    private void sendPurposal () {

        Random rnd = new Random();
        purposeWord(rnd.nextInt( 5 + (int)Math.round((100 - current_level)/(double)10)) + 2);

    }

    private List<String> getStatus (String status) {

        List<String> res = new ArrayList<>();
        res.add(status);

        for (int i = 0; i < status.length(); i++) {

            if (status.charAt(i) != '?') {

                switch(status.charAt(i)) {

                    case 'a':
                        res.add(status.substring(0, i) + "á" + status.substring(i+1));
                        break;
                    case 'e':
                        res.add(status.substring(0, i) + "é" + status.substring(i+1));
                        break;
                    case 'i':
                        res.add(status.substring(0, i) + "í" + status.substring(i+1));
                        break;
                    case 'o':
                        res.add(status.substring(0, i) + "ó" + status.substring(i+1));
                        break;
                    case 'u':
                        res.add(status.substring(0, i) + "ú" + status.substring(i+1));
                        break;
                    default:
                        break;

                }
            }

        }

        return res;
    }

    private void purposeWord (final int to) {

        (new Thread() {
            @Override
            public void run() {

                System.out.println("To: " + to);

                if (current_level >= 100) {

                    try {


                        Thread.sleep(to * 1000);

                        try {

                            Counter punt = new Counter();
                            JSONParser parse = new JSONParser();
                            found = 0;

                            for (String stat : getStatus(status)) {

                                //System.out.println("Stat: " + stat);

                                URI uri = new URI("http", "api.datamuse.com", "/words", "sp=" + stat + "&" + (lang.equals("es") ? "v=es" : ""), null);
                                URL url = new URL(uri.toASCIIString());

                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

//                            System.out.println(conn.getURL());

                                conn.setRequestMethod("GET");
                                conn.connect();

                                int responsecode = conn.getResponseCode();

                                //System.out.println(conn.getResponseMessage());

                                if (responsecode != 200) {

                                    System.out.println("Bad response code!");
                                    throw new RuntimeException("HttpResponseCode: " + responsecode);

                                } else {


                                    BufferedReader entrada = new BufferedReader(new InputStreamReader(url.openStream()));
                                    String chunk = "";
                                    String NL = System.getProperty("line.separator");
                                    StringBuffer res = new StringBuffer("");

                                    while ((chunk = entrada.readLine()) != null) {
                                        res.append(chunk + NL);
                                    }

                                    entrada.close();
                                    conn.disconnect();

                                    //System.out.println(res.toString());

                                    JSONArray array = (JSONArray) parse.parse(res.toString());
                                    System.out.println("Results for " + stat + ": " + array.size());
                                    found += array.size();
                                    JSONObject obj;
                                    String cand, aux;
                                    List<String> cands;

                                    for (int i = 0; i < array.size(); i++) {

                                        obj = (JSONObject) array.get(i);
                                        word = (String) obj.get("word");

                                        if (!word.matches("[a-z á é í ó ú ñ]*"))
                                            continue;

                                        cands = new ArrayList<>();
                                        //System.out.println("Candidate: " + word);

                                        for (int k = 0; k < word.length(); k++) {

                                            cand = "" + word.charAt(k);

                                            switch (cand) {

                                                case "á":
                                                    aux = "a";
                                                    break;
                                                case "é":
                                                    aux = "e";
                                                    break;
                                                case "í":
                                                    aux = "i";
                                                    break;
                                                case "ó":
                                                    aux = "o";
                                                    break;
                                                case "ú":
                                                    aux = "u";
                                                    break;
                                                default:
                                                    aux = cand;
                                                    break;
                                            }

                                            if (!tried.contains(aux) && !cands.contains(aux)) {

                                                //System.out.println("Scoring " + (long) obj.get("score") + " to " + aux);
                                                cands.add(aux);
                                                punt.addPoint(aux, (long) obj.get("score"));

                                            }

                                        }

                                    }

                                }

                            }

                            choosen = punt.getMax();

                        } catch (Exception ex) {

                            ex.printStackTrace();

                        }

                        context.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                //System.out.println("Words found: " + found + ", last: " + word);

                                if (found == 1 && !solves.contains(word))
                                    testWord(word);
                                else
                                    testChoice(choosen);
                            }
                        });

                    } catch (Exception ex) {

                        ex.printStackTrace();

                    }

                } else {

                    try {

                        Thread.sleep(to * 1000);

                        current_level += 10;

                        Random rnd = new Random();

                        choosen = "" + (char) (rnd.nextInt(26) + 'a');

                        while (tried.contains(choosen))
                            choosen = "" + (char) (rnd.nextInt(26) + 'a');

                        context.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                testChoice(choosen);
                            }
                        });

                    } catch (Exception ex) {

                        ex.printStackTrace();

                    }
                }
            }
        }).start();

    }

    public void testWord (String word) {

        System.out.println("Testing " + word);

        this.solves.add(word);

        if (context.__stringsEqual(word, this.secret) || context.__stringsEqual(this.secret, word)) {

            this.status = word;
            this.sendSuccess(word);
            context.oppeonentEnded();


        } else
            this.sendMistake();

        System.out.println("Status " + this.status);

    }

    public void testChoice (String choosen) {

        System.out.println("Testing " + choosen);

        if (!this.tried.contains(choosen) && !this.context.finished) {

            String new_status = "";

            this.tried.add(choosen);

            if (this.context.isMyChar(choosen)) {

                for (int i = 0; i < this.secret.length(); i++) {

                    if (this.status.charAt(i) != '?')

                        new_status += this.status.charAt(i);

                    else {

                        if (context.__isEqual(choosen, this.secret.substring(i, i + 1)))
                            new_status += choosen;//this.secret.substring(i, i + 1);
                        else
                            new_status += "?";
                    }
                }

                this.status = new_status;
                System.out.println("Status " + this.status);

                this.sendSuccess(choosen);

                if (context.__stringsEqual(this.status, this.secret))
                    context.oppeonentEnded();

            } else
                this.sendMistake();

            if (!this.context.finished)
                sendPurposal();
        }
    }

    public IA (MatchActivity context, String secret) {

        this.status = "";
        this.secret = secret;

        for (int i = 0; i < secret.length(); i++) {

            if (context.__isEquals(secret.substring(i, i+1), secret.substring(0, 1)))
                this.status += secret.substring(i, i+1);
            else
                this.status += "?";
        }

        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.pref_file), Context.MODE_PRIVATE);

        int defaultValue = context.getResources().getInteger(R.integer.default_ia_level);
        current_level = sharedPref.getInt(context.getString(R.string.ia_level), defaultValue);

        String defaultLang = sharedPref.getString(context.getString(R.string.def_language), context.getResources().getString(R.string.def_lang));
        this.lang = sharedPref.getString(context.getString(R.string.language), defaultLang);

        this.tried.add(secret.substring(0, 1));

        sendPurposal();

        this.context = context;
    }
}
