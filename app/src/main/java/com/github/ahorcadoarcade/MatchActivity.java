package com.github.ahorcadoarcade;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MatchActivity extends AppCompatActivity implements MessageGetter{

    String word;
    String secret;
    static Map<Integer, String> code_to_key = new HashMap<Integer, String>();
    static Map<String, Integer> key_to_code = new HashMap<String, Integer>();
    
    List<TextView> letters = new ArrayList<>();
    List<TextView> letters_user = new ArrayList<>();
    Integer failures = 0;
    Integer opponent_failures = 0;
    Boolean finished = false;

    String guid;
    Boolean isHuman;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    public MatchActivity() {

        this.code_to_key.put(113, "q");
        this.code_to_key.put(119, "w");
        this.code_to_key.put(101, "e");
        this.code_to_key.put(114, "r");
        this.code_to_key.put(116, "t");
        this.code_to_key.put(121, "y");
        this.code_to_key.put(117, "u");
        this.code_to_key.put(105, "i");
        this.code_to_key.put(111, "o");
        this.code_to_key.put(112, "p");

        this.code_to_key.put(97, "a");
        this.code_to_key.put(115, "s");
        this.code_to_key.put(100, "d");
        this.code_to_key.put(102, "f");
        this.code_to_key.put(103, "g");
        this.code_to_key.put(104, "h");
        this.code_to_key.put(106, "j");
        this.code_to_key.put(107, "k");
        this.code_to_key.put(108, "l");
        this.code_to_key.put(241, "ñ");

        this.code_to_key.put(122, "z");
        this.code_to_key.put(120, "x");
        this.code_to_key.put(99, "c");
        this.code_to_key.put(118, "v");
        this.code_to_key.put(98, "b");
        this.code_to_key.put(110, "n");
        this.code_to_key.put(109, "m");
        this.code_to_key.put(323, "-");
        this.code_to_key.put(325, "SOL");
        this.code_to_key.put(322, "");

        this.key_to_code.put("q", 113);
        this.key_to_code.put("w", 119);
        this.key_to_code.put("e", 101);
        this.key_to_code.put("é", 101);
        this.key_to_code.put("r", 114);
        this.key_to_code.put("t", 116);
        this.key_to_code.put("y", 121);
        this.key_to_code.put("u", 117);
        this.key_to_code.put("ú", 117);
        this.key_to_code.put("i", 105);
        this.key_to_code.put("í", 105);
        this.key_to_code.put("o", 111);
        this.key_to_code.put("ó", 111);
        this.key_to_code.put("p", 112);

        this.key_to_code.put("a", 97);
        this.key_to_code.put("á", 97);
        this.key_to_code.put("s", 115);
        this.key_to_code.put("d", 100);
        this.key_to_code.put("f", 102);
        this.key_to_code.put("g", 103);
        this.key_to_code.put("h", 104);
        this.key_to_code.put("j", 106);
        this.key_to_code.put("k", 107);
        this.key_to_code.put("l", 108);
        this.key_to_code.put("ñ", 241);

        this.key_to_code.put("z", 122);
        this.key_to_code.put("x", 120);
        this.key_to_code.put("c", 99);
        this.key_to_code.put("v", 118);
        this.key_to_code.put("b", 98);
        this.key_to_code.put("n", 110);
        this.key_to_code.put("m", 109);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_match);

        Intent intent = getIntent();
        this.secret = intent.getStringExtra("com.github.ahorcadoarcade.OPONENT");
        this.word = intent.getStringExtra("com.github.ahorcadoarcade.SECRET");
        this.isHuman = intent.getStringExtra("com.github.ahorcadoarcade.TYPE").equals("human");

        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.pref_file), Context.MODE_PRIVATE);

        this.guid = sharedPref.getString(getString(R.string.guid), "NO_GUID");

        Keyboard mKeyboard= new Keyboard(this,R.xml.keyboard);
        kbdView mKeyboardView= (kbdView)findViewById(R.id.keyboardview);
        mKeyboardView.appendOkKey(this.key_to_code.get(this.secret.substring(0, 1)));
        mKeyboardView.setKeyboard( mKeyboard );
        mKeyboardView.setVisibility(View.VISIBLE);
        mKeyboardView.setEnabled(true);
        mKeyboardView.setPreviewEnabled(false);
        mKeyboardView.setOnKeyboardActionListener(new kbdListener(this, mKeyboardView)); /* this, mKeyboardView)*/

        try {

            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.bucket);
            TextView tv, space;

            for (int i = 0; i < this.secret.length(); i++) {

                space = new TextView(this);

                space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                space.setText(" ");
                space.setVisibility(View.VISIBLE);
                space.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35);
                space.setGravity(Gravity.CENTER);

                tv = new TextView(this);

                tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                if (__isEquals(this.secret.substring(i, i + 1), this.secret.substring(0, 1)))
                    tv.setText(this.secret.substring(i, i + 1).toUpperCase());
                else
                    tv.setText("_");

                tv.setVisibility(View.VISIBLE);
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35);
                tv.setGravity(Gravity.CENTER);

                this.letters.add(tv);

                linearLayout.addView(tv);
                linearLayout.addView(space);
            }

            LinearLayout linearLayoutUser = (LinearLayout) findViewById(R.id.bucketmine);


            for (int i = 0; i < this.word.length(); i++) {

                space = new TextView(this);

                space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                space.setText(" ");
                space.setVisibility(View.VISIBLE);
                space.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35);
                space.setGravity(Gravity.CENTER);

                tv = new TextView(this);

                tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                tv.setText(this.word.substring(i, i + 1).toUpperCase());

                if (__isEquals(word.substring(i, i + 1), this.word.substring(0, 1)))
                   tv.setBackgroundColor(0xff00ff00);

                tv.setVisibility(View.VISIBLE);
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35);
                tv.setGravity(Gravity.CENTER);

                this.letters_user.add(tv);

                linearLayoutUser.addView(tv);
                linearLayoutUser.addView(space);
            }


        } catch( Exception ex) {

            ex.printStackTrace();

        }

        if (!this.isHuman)
            new IA(this, this.word);
        else
            mSocket.on("match", new EventListener(this));

    }

    public Boolean handleCompletion () {

        if (this.failures == 10) {

            /* Lost*/

            __showSol();

            if (this.isHuman) {
                mSocket.emit("match_finished", this.guid);
                mSocket.off("match");
            }

            this.finished = true;
            new Thread(new deferredStop(this, 5)).start();

            return true;

        } else {

            /* Won */

            int i = 0;
            for (; i < this.secret.length(); i++) {

                if (!this.letters.get(i).getText().toString().equals(this.secret.substring(i, i + 1).toUpperCase()))
                    break;
            }

            if (i == this.secret.length()) {

                this.finished = true;

                if (this.isHuman) {

                    mSocket.emit("match_finished", this.guid);
                    mSocket.off("match");
                }

                new Thread(new deferredStop(this, 5)).start();
                return true;

            } else
                return false;
        }
    }

    /* PRIVATE */

    private void __setImage() {


        if (this.failures <= 10) {

            ImageView im = (ImageView) this.findViewById(R.id.monigote);
            int res = getResources().getIdentifier("ahorcado_" + this.failures, "mipmap", this.getPackageName());
            im.setImageResource(res);

        }
    }

    private void __showSol () {

        for (int i = 0; i < this.secret.length(); i++)
            this.letters.get(i).setText(this.secret.substring(i, i+1).toUpperCase());

    }

    /* a.length() == 1 */
    public Boolean __contains (String a, String b) {

        int i = 0;
        for (; i < b.length(); i++) {

            if (__isEqual(a, b.substring(i, i + 1)))
                return true;

        }

        return false;
    }

    public Boolean __stringsEqual (String a, String b) {

        if (a.length() != b.length())
            return false;
        else {

            int i = 0;
            for (; i < a.length(); i++) {

                if (!__isEqual(a.substring(i, i+1), b.substring(i, i+1)))
                    return false;

            }

            return true;
        }

    }

    /* length a == length b == 1*/
    public Boolean __isEqual (String a, String b) {

        if (a.equals(b))
            return true;
        else {

            switch (b) {
                case "á":
                    return a.equals("a");
                case "é":
                    return a.equals("e");
                case "í":
                    return a.equals("i");
                case "ó":
                    return a.equals("o");
                case "ú":
                    return a.equals("u");
                default:
                    return false;
            }
        }
    }

    public Boolean __isEquals (String a, String b) {

        return __isEqual(a, b) || __isEqual(b, a);
    }

    /* END PRIVATE */

    /* USER */

    public Boolean isValidCode(int keyCode) {

        /* TO-DO: Inform opponent */

        String label = this.code_to_key.get(keyCode);

        if (__contains(label, this.secret)) {

            for (int i = 0; i < this.secret.length(); i++) {

                if (__isEqual(label, this.secret.substring(i, i+1)))
                    this.letters.get(i).setText(this.secret.substring(i, i+1).toUpperCase());

            }

            if (this.isHuman)
                mSocket.emit("tried_char", this.guid, label, "success");

            return true;

        } else {

            this.failures ++;

            __setImage();

            if (this.isHuman)
                mSocket.emit("tried_char", this.guid, label, "failed");

            return false;
        }
    }

    public void solvingQuest (Boolean solve) {

        EditText editText = (EditText) findViewById(R.id.propuesta);
        editText.setText("");

        LinearLayout lo =  (LinearLayout) findViewById(R.id.solvelay);
        lo.setVisibility(solve ? View.VISIBLE : View.GONE);

    }

    public void solveKey (int keyCode) {

        EditText editText = (EditText) findViewById(R.id.propuesta);
        String solution = editText.getText().toString();

        if (keyCode == 323) {

            if (solution != null && solution.length() > 0)
                editText.setText(solution.substring(0, solution.length() - 1));
            else
                editText.setText("");
        } else
            editText.setText(solution.concat(this.code_to_key.get(keyCode)));
    }

    public void solveWord (View view) {

        /*TO-DO: Inform opponent */

        EditText editText = (EditText) findViewById(R.id.propuesta);
        String solution = editText.getText().toString();

        if (__stringsEqual(solution, this.secret)) {

            for (int i = 0; i < this.secret.length(); i++)
                this.letters.get(i).setText(this.secret.substring(i, i + 1).toUpperCase());

            if (this.isHuman)
                mSocket.emit("tried_char", this.guid, solution, "success");

            new Thread(new deferredStop(this, 5)).start();

        } else {

            this.failures ++;

            __setImage();

            if (failures == 10) {

                __showSol();
                this.finished = true;

                if (this.isHuman)
                    mSocket.emit("tried_char", this.guid, solution, "failed");

                new Thread(new deferredStop(this, 5)).start();

            }
        }
    }

    /* END USER */

    /* AI */

    public Boolean isMyChar (String chr) {

        return __contains(chr, this.word);
    }

    public Boolean isMyWord (String word) {

        return __stringsEqual(word, this.word) || __stringsEqual(this.word, word);
    }

    /* END AI */

    /* OPPONENT */

    /* TO-DO: Receive notifications (socket-io?) from opponent and act accordingly */

    public void opponentMistake () {

        this.opponent_failures ++;

        if (this.opponent_failures <= 10) {

            ImageView im = (ImageView) this.findViewById(R.id.monigote_op);
            int res = getResources().getIdentifier("op_ahorcado_" + this.opponent_failures, "mipmap", this.getPackageName());
            im.setImageResource(res);

        } else {

            this.finished = true;
            __showSol();
            new Thread(new deferredStop(this, 5)).start();

        }

    }

    public void opponentSuccess (String ok) {

        System.out.println("Opponent Success: " + ok);

        if (ok.length() == 1) {

            /* Chararacter success */
            for (int i = 0; i < this.word.length(); i++) {

                if (__isEqual(ok, word.substring(i, i+1)))
                    this.letters_user.get(i).setBackgroundColor(0xff00ff00);
            }

        } else {

            /* Solve success */
            for (int i = 0; i < this.word.length(); i++) {

                this.letters_user.get(i).setBackgroundColor(0xff00ff00);

            }
        }

        //handleCompletionOponent
    }

    public void oppeonentEnded () {

        this.finished = true;
        __showSol();
        new Thread(new deferredStop(this, 5)).start();
        mSocket.off("match");

    }

    @Override
    public void onMessageReceived(final Object... args) {

            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {

                        JSONObject data = (JSONObject) args[0];
                        System.out.println(data);
                        String id = (String) data.get("id");

                        if (id.equals(guid)) {

                            String action = (String) data.get("action");

                            switch(action) {
                                case "success":

                                    String word = (String) data.get("word");
                                    opponentSuccess(word);

                                    break;
                                case "failed":
                                    opponentMistake();
                                    break;

                                case "finished":
                                    oppeonentEnded();
                                    break;

                                default:
                                    break;

                            }
                        }

                    } catch(Exception ex) {

                        ex.printStackTrace();

                    }
                }
            });




    }

    /* END OPPONENT */

    public void onBackPressed() {

        this.finished = true;
        finish();

    }
}
