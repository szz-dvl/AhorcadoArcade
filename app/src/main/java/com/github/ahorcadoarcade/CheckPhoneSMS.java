package com.github.ahorcadoarcade;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import java.util.UUID;

public class CheckPhoneSMS extends AppCompatActivity {

    String code;
    Boolean modify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_check_phone_sms);

        Intent intent = getIntent();
        String phone = intent.getStringExtra("com.github.ahorcadoarcade.PHONE");

        if (phone != null) {

            EditText et = findViewById(R.id.telefono);
            et.setText(phone);

        }

        code = intent.getStringExtra("com.github.ahorcadoarcade.CODE");

        if (code == null)
            code = UUID.randomUUID().toString().substring(0, 4).toLowerCase();

        modify = intent.getBooleanExtra("com.github.ahorcadoarcade.FROM_CFG", false);
    }

    public void checkPhone (View view) {

        EditText text = findViewById(R.id.telefono);
        String phone = text.getText().toString().trim();

        sendSMS(phone, "AhorcadoArcade: " + code);

        Intent intent = new Intent(this, CheckSMSCode.class);
        intent.putExtra("com.github.ahorcadoarcade.PHONE", phone);
        intent.putExtra("com.github.ahorcadoarcade.CODE", code);
        intent.putExtra("com.github.ahorcadoarcade.FROM_CFG", modify);

        startActivity(intent);
        finish();

    }

    private void sendSMS(String phoneNumber, String message) {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }

}
