package com.github.ahorcadoarcade;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.view.View;

public class kbdListener extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener {

    private kbdView kv;
    private Keyboard kbd;
    private MatchActivity parent;
    private Boolean ended = false;

    public kbdListener (MatchActivity context, kbdView view)
    {
        super();
        this.parent = context;
        this.kv = view;
        this.kbd = view.getKeyboard();
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {

        //System.out.println("Got key!");

        try {

            if (primaryCode == 325) {

                kv.solving = !kv.solving;
                kv.invalidateAllKeys();

                this.parent.solvingQuest(kv.solving);

                return;

            } else if (!kv.solving) {

                if (primaryCode < 320 && !this.ended) {

                    if (kv.wasPressed(primaryCode))
                        return;
                    else {

                        if (this.parent.isValidCode(primaryCode))
                            kv.appendOkKey(primaryCode);
                        else
                            kv.appendBadKey(primaryCode);

                        this.ended = this.parent.handleCompletion();
                    }
                }

            } else {

                this.parent.solveKey(primaryCode);

            }

        }  catch (Exception ex) {

            ex.printStackTrace();
            return;
        }

        //TO-DO invalidate on kbd

        return;
    }

    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {
    }
}
