package com.github.ahorcadoarcade;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.ClipData;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SharedView extends ViewModel {

    protected static class FragModel {
        String text;
        String uri;
        String lookup;

        public FragModel (String t, String pu, String lu) {

            text = t;
            uri = pu;
            lookup = lu;

        }

    }

    private final MutableLiveData<FragModel> selected = new MutableLiveData<>();

    public void select(FragModel item) {
        selected.setValue(item);
    }

    public LiveData<FragModel> getSelected() {
        return selected;
    }
}
