package com.github.ahorcadoarcade;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class CheckSMSCode extends AppCompatActivity {

    String phone;
    String code;
    Boolean modify;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_check_smscode);

        Intent intent = getIntent();
        phone = intent.getStringExtra("com.github.ahorcadoarcade.PHONE");
        code = intent.getStringExtra("com.github.ahorcadoarcade.CODE");
        modify = intent.getBooleanExtra("com.github.ahorcadoarcade.FROM_CFG", false);


    }

    public void checkCode (View view) {

        EditText text = findViewById(R.id.codigo);
        String codigo = text.getText().toString().trim().toLowerCase();
        Intent intent;

        if (codigo.equals(code)) {

            SharedPreferences sharedPref = this.getSharedPreferences(
                    getString(R.string.pref_file), Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.phone), phone);
            editor.apply();

            String guid = sharedPref.getString(getString(R.string.guid), "NO_GUID");
            mSocket.emit("new_phone", guid, phone);

            if (!modify) {

                intent = new Intent(this, MainActivity.class);
                startActivity(intent);

            }


        } else {

            intent = new Intent(this, CheckPhoneSMS.class);
            intent.putExtra("com.github.ahorcadoarcade.PHONE", phone);
            intent.putExtra("com.github.ahorcadoarcade.CODE", code);
            intent.putExtra("com.github.ahorcadoarcade.FROM_CFG", modify);

            startActivity(intent);
        }


        finish();
    }
}
