package com.github.ahorcadoarcade;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class DetailsFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>, Timer, MessageGetter {

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    SharedPreferences sharedPref;
    String guid;

    private int spent = 0;
    private AsyncTask to;
    private Boolean waiting = false;

    // Defines a constant that identifies the loader
    static final int DETAILS_QUERY_ID = 0;

    // Defines the selection clause
    //private static final String SELECTION = ContactsContract.Data.LOOKUP_KEY + " = ?";

    private static final String SELECTION =
            ContactsContract.Data.LOOKUP_KEY + " = ?" +
                    " AND " +
                    ContactsContract.Data.MIMETYPE + " = " +
                    "'" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE+ "'";


    private static final String[] PROJECTION =
            {
                    ContactsContract.CommonDataKinds.Phone._ID,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.TYPE
            };

    // Defines the array to hold the search criteria
    private String[] mSelectionArgs = {""};

    /*
     * Defines a variable to contain the selection value. Once you
     * have the Cursor from the Contacts table, and you've selected
     * the desired row, move the row's LOOKUP_KEY value into this
     * variable.
     */
    private String mLookupKey = ""; //lateinit var mLookupKey: String

    private static final String SORT_ORDER = ContactsContract.CommonDataKinds.Phone.NUMBER + " ASC ";

    private JSONArray currentPhones;

    private SharedView.FragModel fm;

    // A UI Fragment must inflate its View
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedView model = ViewModelProviders.of(getActivity()).get(SharedView.class);
        sharedPref = getActivity().getSharedPreferences(
                getString(R.string.pref_file),
                Context.MODE_PRIVATE
        );

        guid = sharedPref.getString(getString(R.string.guid), "NO_GUID");

        model.getSelected().observe(this, item -> {

            if (!waiting) {

                fm = (SharedView.FragModel) item;

                TextView tv = getActivity().findViewById(R.id.contact_name_detail);
                tv.setText(fm.text);

                ImageView iv = getActivity().findViewById(R.id.imgContactDetail);

                if (fm.uri.length() > 0)
                    iv.setImageURI(Uri.parse(fm.uri));
                else
                    iv.setImageResource(R.drawable.somebody);

                getActivity().findViewById(R.id.info_detail).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.ph_detail).setVisibility(View.GONE);

                mLookupKey = fm.lookup;
                getLoaderManager().restartLoader(DETAILS_QUERY_ID, null, this);
            }

        });

        // Inflate the fragment layout
        View view = inflater.inflate(R.layout.details_fragment,
                container, false);

        Button button = (Button) view.findViewById(R.id.retarC);
        button.setOnClickListener(v -> {

            waiting = true;

            String phone = sharedPref.getString(getString(R.string.phone), "");
            mSocket.emit("challenge", guid, phone, currentPhones);

            TextView tv = getActivity().findViewById(R.id.retarT);
            getActivity().findViewById(R.id.retarC).setVisibility(View.GONE);

            tv.setText(getResources().getString(R.string.waiting_for, fm.text));
            tv.setVisibility(View.VISIBLE);

            getActivity().findViewById(R.id.retarB).setVisibility(View.VISIBLE);
            to = new timeOut(this).execute(1000);

        });

        mSocket.on("challenge", new EventListener(this));

        return view;

    }

    private void setDetailStatus () {

        mSocket.emit("is_online", guid, currentPhones, (Ack) args -> {

            getActivity().runOnUiThread(() -> {
                try {

                    ProgressBar pb = (ProgressBar) getActivity().findViewById(R.id.retarB);
                    pb.setVisibility(View.GONE);
                    pb.setProgress(0);

                    Boolean res = (Boolean) args[0];

                    TextView tv = getActivity().findViewById(R.id.retarT);

                    if (res) {

                        getActivity().findViewById(R.id.retarC).setVisibility(View.VISIBLE);
                        tv.setVisibility(View.GONE);

                    } else {

                        getActivity().findViewById(R.id.retarC).setVisibility(View.GONE);
                        tv.setText(getResources().getString(R.string.not_online, fm.text));
                        tv.setVisibility(View.VISIBLE);

                    }

                } catch (Exception ex) {

                    ex.printStackTrace();
                }
            });
        });


    }

    @Override
    public void onTimeout() {

        /* Start AI if server is down. */
        spent ++;

        ProgressBar pb = (ProgressBar) getActivity().findViewById(R.id.retarB);

        if (spent > 60) {

            waiting = false;
            spent = 0;

            setDetailStatus();

        } else {

            pb.setProgress((int) Math.round(((double)spent/60) * 100));
            to = new timeOut(this).execute(1000);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Initializes the loader framework
        getLoaderManager().initLoader(DETAILS_QUERY_ID, null, this);


    }

//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//
//        if (fm != null) {
//
//            System.out.println("Saving instance!");
//
//            outState.putString("CONTACT", fm.text);
//            outState.putString("PHOTO", fm.uri);
//            outState.putString("KEY", fm.lookup);
//        }
//
//        // call superclass to save any view hierarchy
//        super.onSaveInstanceState(outState);
//    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {

        CursorLoader mLoader;

        // Choose the proper action
        switch (loaderId) {
            case DETAILS_QUERY_ID:

                // Assigns the selection parameter
                mSelectionArgs[0] = mLookupKey;

                // Starts the query
                mLoader = new CursorLoader(
                        getActivity(),
                        ContactsContract.Data.CONTENT_URI,
                        PROJECTION,
                        SELECTION,
                        mSelectionArgs,
                        SORT_ORDER
                );
                break;
            default:
                mLoader = null;
                break;
        }

        return mLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor phones) {

        switch (loader.getId()) {
            case DETAILS_QUERY_ID:

                /*
                 * Process the resulting Cursor here.
                 */

                    currentPhones = getUniquePhones(phones);

                    if (fm != null) {

                        setDetailStatus();

                        //Otherwise this function is called again onResume() besides the lifecycle.
                        getLoaderManager().destroyLoader(0);
                    }

                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        switch (loader.getId()) {
            case DETAILS_QUERY_ID:
                /*
                 * If you have current references to the Cursor,
                 * remove them here.
                 */

                break;
            default:
                break;
        }

    }

    private JSONArray getUniquePhones (Cursor phones) {

        JSONArray res = new JSONArray();

        ArrayList<String> aux = new ArrayList<>();

        while (phones.moveToNext()) {

            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            number = number.replace(" ", "");

            if (number.startsWith("+"))
                number = number.substring(3);

            if (!aux.contains(number))
                aux.add(number);
        }

        for (String s : aux)
            res.put(s);

        return res;
    }

    @Override
    public void onMessageReceived(final Object... args) {

        System.out.println("Received challenge: ");

        try {

            JSONObject data = (JSONObject) args[0];

            System.out.println(data);

            String action = (String) data.get("action");
            String id = (String) data.get("id");

            switch (action) {
                case "accept": /* Reto aceptado */

                    if (id.equals(guid)) {

                        Intent intent = new Intent(getActivity(), getWord.class);
                        intent.putExtra("com.github.ahorcadoarcade.CHALLENGE", true);

                        startActivity(intent);

                        if (to != null)
                            to.cancel(true);

                        Activity act = getActivity();

                        if (act != null)
                            act.finish();

                        mSocket.off("challenge");

                    }

                    break;
                case "reject": /* Reto rechazado */

                    if (id.equals(guid)) {

                        if (to != null)
                            to.cancel(true);

                        waiting = false;
                        spent = 0;

                        setDetailStatus();
                    }

                    break;
                default:
                    break;
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }
}

