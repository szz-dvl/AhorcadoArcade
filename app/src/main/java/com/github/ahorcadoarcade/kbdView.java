package com.github.ahorcadoarcade;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.NinePatchDrawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.List;

public class kbdView extends KeyboardView {

    Context context;
    List<Integer>badKeys;
    List<Integer>okKeys;
    Paint paint;
    Boolean solving;

    @Override
    public void onDraw(Canvas canvas) {
        //super.onDraw(canvas);

          //System.out.println("Drawing kbd");
//        System.out.println(okKeys);

            try {

                Drawable dr;
                this.paint.setColor(Color.parseColor("#ffffff"));

                for (Keyboard.Key key : getKeyboard().getKeys()) {


                    if (this.solving) {

                        if (key.pressed || key.codes[0] == 325)
                            dr = (Drawable) context.getResources().getDrawable(R.drawable.gray_rectangle);
                        else
                            dr = (Drawable) context.getResources().getDrawable(R.drawable.black_rectangle);

                    } else {

                        if (this.isBadKey(key.codes[0])) {

                            dr = (Drawable) context.getResources().getDrawable(R.drawable.red_rectangle);

                        } else if (this.isOkKey(key.codes[0])) {

                            dr = (Drawable) context.getResources().getDrawable(R.drawable.green_rectangle);

                        } else {

                            dr = (Drawable) context.getResources().getDrawable(R.drawable.black_rectangle);
                        }
                    }


                    dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
                    dr.draw(canvas);

                    if (key.codes[0] == 323) {

                        if (this.solving)
                            canvas.drawText("<", key.x + (key.width / 2), key.y + (key.height / 2), this.paint);
                        else
                            canvas.drawText("", key.x + (key.width / 2), key.y + (key.height / 2), this.paint);

                    } else
                        canvas.drawText(key.label.toString(), key.x + (key.width / 2), key.y + (key.height / 2), this.paint);
                }

            } catch (Exception ex) {

                ex.printStackTrace();

            }
    }

    public void appendBadKey (Integer keycode) {

        this.badKeys.add(keycode);

    }

    public void removeBadKey (Integer keycode) {

        this.badKeys.remove(keycode);

    }

    public Boolean isBadKey (Integer keycode) {

        return this.badKeys.contains(keycode);

    }

    public void appendOkKey (Integer keycode) {

        this.okKeys.add(keycode);

    }

    public void removeOkKey (Integer keycode) {

        this.okKeys.remove(keycode);

    }

    public Boolean isOkKey (Integer keycode) {

        return this.okKeys.contains(keycode);

    }

    public Boolean wasPressed (Integer keycode) {

        return this.isOkKey(keycode) || this.isBadKey(keycode);

    }
    public kbdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.badKeys = new ArrayList<>();
        this.okKeys = new ArrayList<>();
        this.paint = new Paint();
//        Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/Hippie.otf");
//        paint.setTypeface(font);
        this.paint.setTextAlign(Paint.Align.CENTER);
        this.paint.setTextSize(30);
        this.solving = false;

    }
}
