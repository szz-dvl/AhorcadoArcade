package com.github.ahorcadoarcade;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.EditText;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Date;
import java.util.Random;

public class PrepareMatch extends AppCompatActivity implements RaeGetter, WordGetter, MessageGetter, Timer{

    String word;
    String secret;
    String lang;
    String guid;
    AsyncTask to;
    Date start_t;
    Integer left = 10000;
    Boolean challenge;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_prepare_match);

        Boolean alive = false;
        Integer elapsed = 0;

        Intent intent = getIntent();
        challenge = intent.getBooleanExtra("com.github.ahorcadoarcade.CHALLENGE", false);

        if (savedInstanceState != null) {
            alive = savedInstanceState.getBoolean("PREPARE_DONE");
            elapsed = savedInstanceState.getInt("ELAPSED_TIME");
            left = savedInstanceState.getInt("LEFT_TIME");

        }

        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.pref_file), Context.MODE_PRIVATE);

        String defaultLang = sharedPref.getString(getString(R.string.def_language), getResources().getString(R.string.def_lang));
        this.lang = sharedPref.getString(getString(R.string.language), defaultLang);

        this.secret = intent.getStringExtra("com.github.ahorcadoarcade.SECRET");

        this.guid = sharedPref.getString(getString(R.string.guid), "NO_GUID");

        if (!alive) {

            mSocket.on("broadcast", new EventListener(this));

            if (challenge)
                mSocket.emit("challenge_pairing", this.guid, this.secret);
            else
                mSocket.emit("match_pairing", this.guid, this.secret);

        }

        if (!challenge) {

            start_t = new Date();
            to = new timeOut(this).execute(left - elapsed);

        }
    }

    @Override
    protected void onStop() {

        super.onStop();

        if (!challenge)
            to.cancel(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("PREPARE_DONE", true);
        int diff = (int) (new Date().getTime() - start_t.getTime());
        outState.putInt("ELAPSED_TIME", diff);
        outState.putInt("LEFT_TIME", left - diff);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onWordChecked(Boolean res) {

        if (res) {

            Intent intent = new Intent(this, MatchActivity.class);
            intent.putExtra("com.github.ahorcadoarcade.OPONENT", this.word);
            intent.putExtra("com.github.ahorcadoarcade.SECRET", this.secret);
            intent.putExtra("com.github.ahorcadoarcade.TYPE", "ia");

            mSocket.emit("wait_no_more", this.guid);

            startActivity(intent);
            finish();
            mSocket.off();

        } else {

            Random rnd = new Random();
            new wordGetter(this, this.lang).execute((char) (rnd.nextInt(26) + 'a'));

        }

    }

    @Override
    public void onWordSelected(String word) {

        if (word != null) {

            this.word = word;
            new checkFromRAE(this, this.lang).execute(word);

        } else {

            Random rnd = new Random();
            new wordGetter(this, this.lang).execute((char) (rnd.nextInt(26) + 'a'));
        }

    }

    @Override
    public void onTimeout() {

        /* Start AI if server is down. */

        System.out.println("Unpaired, starting IA");
        Random rnd = new Random();
        new wordGetter(this, lang).execute((char) (rnd.nextInt(26) + 'a'));

    }

    @Override
    public void onMessageReceived(final Object... args) {

        try {

            JSONObject data = (JSONObject) args[0];

            System.out.println(data);

            String action = (String) data.get("action");

            switch (action) {
                case "paired":

                    JSONObject player1 = (JSONObject) data.get("player1");
                    JSONObject player2 = (JSONObject) data.get("player2");

                    String p1_guid = (String) player1.get("id");
                    String p2_guid = (String) player2.get("id");

                    if (p1_guid.equals(guid) || p2_guid.equals(guid)){

                        if (!challenge)
                            this.to.cancel(true);

                        Intent intent = new Intent(this, MatchActivity.class);
                        intent.putExtra("com.github.ahorcadoarcade.SECRET", this.secret);
                        intent.putExtra("com.github.ahorcadoarcade.TYPE", "human");

                        if (p1_guid.equals(guid))
                            intent.putExtra("com.github.ahorcadoarcade.OPONENT", (String) player2.get("secret"));
                        else if (p2_guid.equals(guid))
                            intent.putExtra("com.github.ahorcadoarcade.OPONENT", (String) player1.get("secret"));

                        startActivity(intent);
                        finish();
                        mSocket.off("broadcast");
                    }

                    break;

                    default:
                       break;
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }
}
