package com.github.ahorcadoarcade;

public interface Timer {

    public void onTimeout ();
}
