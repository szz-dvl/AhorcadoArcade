package com.github.ahorcadoarcade;

import android.app.Activity;

public interface MessageGetter {

    void onMessageReceived (Object... args);

}
