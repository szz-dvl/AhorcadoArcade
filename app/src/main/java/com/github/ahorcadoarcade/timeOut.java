package com.github.ahorcadoarcade;

import android.os.AsyncTask;

public class timeOut extends AsyncTask<Integer, Void, Boolean> {

    Timer context;

    public timeOut (Timer context) {

        this.context = context;

    }

    @Override
    protected Boolean doInBackground(Integer... millis) {
        try {

            if (millis[0] > 0) {

                Thread.sleep(millis[0]);
                return true;

            } else
                return false;

        } catch(InterruptedException ex) {

            return false;

        }
    }

    @Override
    protected void onPostExecute(Boolean to) {

        if (to)
            this.context.onTimeout();

    }
}
