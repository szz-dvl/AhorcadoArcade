package com.github.ahorcadoarcade;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class checkFromRAE extends AsyncTask<String, Void, Boolean> {

    private RaeGetter context;
    private String lang;

    public checkFromRAE (RaeGetter context, String lang){

        this.context = context;
        this.lang = lang;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... texts) {

        String query = "http://www.wordreference.com/" + (this.lang.equals("es") ? "definicion/" : "definition/");

        try {

            Document doc = Jsoup.connect(query.concat(texts[0])).get();

            if (doc.select("#noEntryFound").isEmpty())
                return true;
            else
                return false;

        } catch(Exception ex) {

            ex.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean res) {

        this.context.onWordChecked(res);
    }

}