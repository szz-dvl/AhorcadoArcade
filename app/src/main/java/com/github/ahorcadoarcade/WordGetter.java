package com.github.ahorcadoarcade;

public interface WordGetter {

    void onWordSelected (String word);
}
