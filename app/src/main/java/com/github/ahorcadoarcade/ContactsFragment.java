package com.github.ahorcadoarcade;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.List;

public class ContactsFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener,
        SearchView.OnQueryTextListener {


    /*
     * Defines an array that contains column names to move from
     * the Cursor to the ListView.
     */
    @SuppressLint("InlinedApi")
    private final static String[] FROM_COLUMNS = {
            Build.VERSION.SDK_INT
                    >= Build.VERSION_CODES.HONEYCOMB ?
                    ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
                    ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.Contacts.PHOTO_URI
    };

    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION =
            {
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.LOOKUP_KEY,
                    Build.VERSION.SDK_INT
                            >= Build.VERSION_CODES.HONEYCOMB ?
                            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
                            ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_URI

            };

    // The column index for the _ID column
    private static final int CONTACT_ID_INDEX = 0;

    // The column index for the CONTACT_KEY column
    private static final int CONTACT_KEY_INDEX = 1;

    // The column index for the CONTACT_NAME column
    private static final int CONTACT_NAME_INDEX = 2;

    // The column index for the CONTACT_PHOTO_URI column
    private static final int CONTACT_PHOTO_INDEX = 3;

    // Defines the text expression
    @SuppressLint("InlinedApi")
    private static final String SELECTION =
            (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                    ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " LIKE ?" :
                    ContactsContract.Contacts.DISPLAY_NAME + " LIKE ?")
            + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER + "='1'"
            + " AND (" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
            + ContactsContract.Contacts.DISPLAY_NAME + " != '' )";

    // Defines a variable for the search string
    private String mSearchString = "";

    // Defines the array to hold values that replace the ?
    private String[] mSelectionArgs = { mSearchString };

    /*
     * Defines an array that contains resource ids for the layout views
     * that get the Cursor column contents. The id is pre-defined in
     * the Android framework, so it is prefaced with "android.R.id"
     */
    private final static int[] TO_IDS = {
            R.id.contact_name,
            R.id.imgContact
    };

    // Define global mutable variables
    // Define a ListView object
    ListView mContactsList;

    // Define variables for the contact the user selects
    // The contact's _ID value
    long mContactId;

    // The contact's LOOKUP_KEY
    String mContactKey;

    // A content URI for the selected contact
    Uri mContactUri;

    // An adapter that binds the result Cursor to the ListView
    private SimpleCursorAdapter mCursorAdapter;

    //SearchView to filter candidates
    private SearchView sv;

    //Communicate with DetailsFragment
    private SharedView model;

    // Empty public constructor, required by the system
    public ContactsFragment() {}

    @Override
    public boolean onQueryTextChange(String newText) {

        mSearchString = newText;
        getLoaderManager().restartLoader(0, null, this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;

    }

    // A UI Fragment must inflate its View
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the fragment layout
        return inflater.inflate(R.layout.contacts_list_fragment,
                container, false);
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        model = ViewModelProviders.of(getActivity()).get(SharedView.class);

        // Initializes the loader

        getLoaderManager().initLoader(0, null, this);

        // Gets the ListView from the View list of the parent activity
        mContactsList = (ListView) getActivity().findViewById(R.id.contact_list_view);

        // Gets a CursorAdapter
        mCursorAdapter = new SimpleCursorAdapter(
                getActivity(),
                R.layout.contacts_list_item,
                null,
                FROM_COLUMNS,
                TO_IDS,
                0);

        mCursorAdapter.setViewBinder(new ContactsViewBinder());

        // Sets the adapter for the ListView
        mContactsList.setAdapter(mCursorAdapter);
        mContactsList.setOnItemClickListener(this);

        sv = (SearchView)getActivity().findViewById(R.id.searchView);
        sv.setOnQueryTextListener(this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {

        //return contactsLoader();

        /*
         * Makes search string into pattern and
         * stores it in the selection array
         */
        mSelectionArgs[0] = "%" + mSearchString + "%";
        // Starts the query
        return new CursorLoader(
                getActivity(),
                ContactsContract.Contacts.CONTENT_URI,
                PROJECTION,
                SELECTION,
                mSelectionArgs,
                ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC"
        );
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View item, int position, long rowID) {

        // Get the Cursor
        Cursor cursor = ((AdapterView<SimpleCursorAdapter>) parent).getAdapter().getCursor();

        // Move to the selected contact
        cursor.moveToPosition(position);

        String uri = cursor.getType(CONTACT_PHOTO_INDEX) == 3 ? cursor.getString(CONTACT_PHOTO_INDEX) : "";

        // Get the _ID value
        mContactId = cursor.getLong(CONTACT_ID_INDEX);

        // Get the selected LOOKUP KEY
        mContactKey = cursor.getString(CONTACT_KEY_INDEX);

        // Create the contact's content Uri
        mContactUri = ContactsContract.Contacts.getLookupUri(mContactId, mContactKey);

        //Get name and image uri for slave fragment
        model.select(new SharedView.FragModel(cursor.getString(CONTACT_NAME_INDEX), uri, mContactKey));

        /*
         * You can use mContactUri as the content URI for retrieving
         * the details for a contact.
         */
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Put the result Cursor in the adapter for the ListView

        //System.out.println(contactsFromCursor(cursor));
        mCursorAdapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        // Delete the reference to the existing Cursor
        mCursorAdapter.swapCursor(null);

    }

//    private List<String> contactsFromCursor(Cursor cursor) {
//        List<String> contacts = new ArrayList<String>();
//
//        if (cursor.getCount() > 0) {
//            cursor.moveToFirst();
//
//            do {
//                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                contacts.add(name);
//            } while (cursor.moveToNext());
//        }
//
//        return contacts;
//    }

//    private  Loader<Cursor> contactsLoader() {
//        Uri contactsUri = ContactsContract.Contacts.CONTENT_URI; // The content URI of the phone contacts
//
//        String[] projection = {                                  // The columns to return for each row
//                ContactsContract.Contacts._ID,
//                ContactsContract.Contacts.LOOKUP_KEY,
//                ContactsContract.Contacts.DISPLAY_NAME,
//                ContactsContract.Contacts.PHOTO_URI
//        } ;
//
//        //String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + "='1'";
//        String selection = null;                                 //Selection criteria
//        String[] selectionArgs = {};                             //Selection criteria
//        String sortOrder = null;                                 //The sort order for the returned rows
//
//        return new CursorLoader(
//                getActivity(),
//                contactsUri,
//                projection,
//                selection,
//                selectionArgs,
//                sortOrder);
//    }
}
