package com.github.ahorcadoarcade;

import android.app.Activity;

public class deferredStop implements Runnable {

    Activity context;
    Integer to;

    public deferredStop(Activity context, Integer to) {
        this.context = context;
        this.to = to;
    }

    public void run() {
        try {

            Thread.sleep(this.to * 1000);
            this.context.finish();

        } catch(Exception ex) {

            ex.printStackTrace();
        }
    }
}
