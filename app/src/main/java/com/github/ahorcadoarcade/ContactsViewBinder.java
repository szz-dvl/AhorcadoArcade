package com.github.ahorcadoarcade;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.net.URI;

public class ContactsViewBinder implements SimpleCursorAdapter.ViewBinder {

    @Override
    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {

        if (columnIndex == 3) {

            ImageView iv = (ImageView) view;

            if (cursor.getType(columnIndex) == 3)
                iv.setImageURI(Uri.parse(cursor.getString(columnIndex)));
            else
                iv.setImageResource(R.drawable.somebody);

        } else {

            TextView tv = (TextView) view;
            tv.setText(cursor.getString(columnIndex));

        }

        return true;
    }
}
