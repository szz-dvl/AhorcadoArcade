package com.github.ahorcadoarcade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class ModosJuego extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_modos_juego);
    }


    public void onlineGame (View view) {

        startActivity(new Intent(this, getWord.class));
        finish();

    }

    public void retoGame (View view) {

        startActivity(new Intent(this, ChallengeActivity.class));
        finish();
    }
}
