package com.github.ahorcadoarcade;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class configApp extends AppCompatActivity {

    SharedPreferences sharedPref;
    public Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.1.132");
        } catch (URISyntaxException e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_config_app);

        Intent intent =  getIntent();
        Boolean have_sms = intent.getBooleanExtra("HAVE_SMS", false);

        sharedPref = this.getSharedPreferences(
                getString(R.string.pref_file), Context.MODE_PRIVATE);

        //sharedPref.edit().clear().commit();

        int defaultValue = getResources().getInteger(R.integer.default_ia_level);
        int current_level = sharedPref.getInt(getString(R.string.ia_level), defaultValue);

        SeekBar seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        seekBar.setProgress(current_level);

        ArrayList<SpinnerItemData> list = new ArrayList<>();
        list.add(new SpinnerItemData(getResources().getString(R.string.cast), R.drawable.spanishflag));
        list.add(new SpinnerItemData(getResources().getString(R.string.ingl), R.drawable.english));

        Spinner sp = (Spinner) findViewById(R.id.spinner);
        SpinnerAdapter adapter = new SpinnerAdapter(this,
                R.layout.spinner_layout, R.id.txtspinner, list);

        //adapter.setDropDownViewResource(R.layout.spinner_layout);
        sp.setAdapter(adapter);

        sp.setOnItemSelectedListener(spinnerSelected);

        String defaultLang = sharedPref.getString(getString(R.string.def_language), getResources().getString(R.string.def_lang));
        String lang = sharedPref.getString(getString(R.string.language), defaultLang);

        sp.setSelection(lang.equals("es") ? 0 : 1);

        if (have_sms) {

            TextView tv = findViewById(R.id.phoneChange);
            tv.setText(sharedPref.getString(getString(R.string.phone), ""));

        } else {

            findViewById(R.id.phoneSection).setVisibility(View.GONE);
            findViewById(R.id.phoneNum).setVisibility(View.GONE);
        }
    }


    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // updated continuously as the user slides the thumb
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // called when the user first touches the SeekBar
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(getString(R.string.ia_level), seekBar.getProgress());
            editor.commit();
        }
    };

    Spinner.OnItemSelectedListener spinnerSelected = new Spinner.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parentView, View itemView, int position, long id) {

            if (itemView != null) {

                TextView textView = (TextView) itemView.findViewById(R.id.txtspinner);

                SharedPreferences.Editor editor = sharedPref.edit();
                String lang = textView.getText().toString().equals(getResources().getString(R.string.cast)) ? "es" : "en";
                editor.putString(getString(R.string.language), lang);
                editor.apply();

                mSocket.emit("new-lang", sharedPref.getString(getString(R.string.guid), "NO_GUID"), lang);

            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
            return;
        }

    };

    public void changePhone (View v) {

        Intent intent = new Intent(this, CheckPhoneSMS.class);
        intent.putExtra("com.github.ahorcadoarcade.FROM_CFG", true);

        startActivity(intent);
        finish();

    }
    public void submitSettings (View v) {

        finish();

    }
}
