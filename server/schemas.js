let mongoose = require('mongoose');

let matchSchema = new mongoose.Schema({
	
	player1: String,
	player2: String,
	challenge: { type: Boolean, default: false },
	p1ready: { type: Boolean, default: false },
	p2ready: { type: Boolean, default: false }
	
	
	/* Victory times to manage ties */
});

matchSchema.statics.findOpponent = function (id) {

	return new Promise ((resolve, reject) => {

		this.findOne().or([ {player1: id}, {player2: id} ])
			.then(match => {

				if (match) {

					if (match.player1 == id)
						resolve(match.player2);
					else
						resolve(match.player1);
				} else {
					reject();
				}
			})
			.catch(reject)
	})
}

matchSchema.statics.setReady = function (id) {

	return new Promise ((resolve, reject) => {

		this.findOne().or([ {player1: id}, {player2: id} ])
			.then(match => {

				if (match.player1 == id)
					match.p1ready = true;
				else
					match.p2ready = true;

				match.save(resolve);
				
			})
			.catch(reject)
	})
}

matchSchema.statics.isReady = function (id) {

	return new Promise ((resolve, reject) => {

		this.findOne().or([ {player1: id}, {player2: id} ])
			.then(match => {

				resolve(match.p1ready && match.p2ready)
				
			})
			.catch(reject)
	})
}

matchSchema.statics.deleteMatch = function (id1, id2) {

	return new Promise ((resolve, reject) => {

		this.findOneAndDelete().or([ {player1: id1, player2: id2}, {player1: id2, player2: id1} ])
			.then(resolve)
			.catch(reject)
	})		
}

let playerSchema = new mongoose.Schema({
	id: String,
	lang: String,
	secret: String,
	phone: String,
	socket: String,
	paired: { type: Boolean, default: false },
	pairing: { type: Boolean, default: false },
	checked: { type: Number, default: 0 }
	
});

playerSchema.statics.findById = function (id) {
	
	return new Promise ((resolve, reject) => {

		this.findOne({id: id})
			.then(resolve)
			.catch(reject)
	});
	
}

playerSchema.statics.deletePlayer = function (socket_id) {
	
	return new Promise ((resolve, reject) => {

		this.findOneAndDelete({socket: socket_id})
			.then(resolve)
			.catch(reject)
	});
	
}

playerSchema.statics.findBySocket = function (socket_id) {
	
	return new Promise ((resolve, reject) => {

		this.findOne({socket: socket_id})
			.then(resolve)
			.catch(reject)
	});
	
}

playerSchema.statics.findByPhone = function (phone) {
	
	return new Promise ((resolve, reject) => {

		this.findOne({phone: phone})
			.then(resolve)
			.catch(reject)
	});
	
}

playerSchema.statics.findWaiters = function (cb) {

	return new Promise ((resolve, reject) => {

		this.find({pairing: true, paired: false})
			.then(resolve)
			.catch(reject)
	});
}


module.exports = {

	player: mongoose.model('Player', playerSchema),
	match: mongoose.model('Match', matchSchema),

}
