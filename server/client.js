
/* const io = require('socket.io-client');
const client = io('http://192.168.1.38/', {
  path: '/socket.io'
}); */

/**
 * Fast UUID generator, RFC4122 version 4 compliant.
 * @author Jeff Ward (jcward.com).
 * @license MIT license
 * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
 **/
var UUID = (function() {
	var self = {};
	var lut = []; for (var i=0; i<256; i++) { lut[i] = (i<16?'0':'')+(i).toString(16); }

	self.generate = function() {
		var d0 = Math.random()*0xffffffff|0;
		var d1 = Math.random()*0xffffffff|0;
		var d2 = Math.random()*0xffffffff|0;
		var d3 = Math.random()*0xffffffff|0;
		return lut[d0&0xff]+lut[d0>>8&0xff]+lut[d0>>16&0xff]+lut[d0>>24&0xff]+'-'+
			lut[d1&0xff]+lut[d1>>8&0xff]+'-'+lut[d1>>16&0x0f|0x40]+lut[d1>>24&0xff]+'-'+
			lut[d2&0x3f|0x80]+lut[d2>>8&0xff]+'-'+lut[d2>>16&0xff]+lut[d2>>24&0xff]+
			lut[d3&0xff]+lut[d3>>8&0xff]+lut[d3>>16&0xff]+lut[d3>>24&0xff];
	}

	return self;
	
})();

const client = require('socket.io-client')('http://192.168.1.132');
let fake_id = "santihfsjdsfjh-7890-sdasd-34543"
let fake_lang = "es";
let fake_phone = "666666666";
let secret = null;

function ask(text) {

	process.stdin.resume();
	process.stdout.write(text);

	return new Promise (
		resolve => {
			
			process.stdin.once('data', data => {
				resolve(data.toString().trim());
			});
				
		}
	)
}

function sendCmd () {

	ask("Provide command: ")
		.then(cmd => {

			switch (cmd) {
				
				case "lang":
					
					ask("Provide lang: ")
						.then(lang => {
							
							client.emit('new-lang', fake_id, lang);
							sendCmd();

						})
					
						break;

				case "pair":
					
					ask("Provide word: ")
						.then(word => {
							
							client.emit('match_pairing', fake_id, word);
							sendCmd();
						})
					
						break;

				case "reto":
					
					ask("Provide phone: ")
						.then(phone => {
							
							client.emit('challenge', fake_id, phone, ['611130202']);
							sendCmd();
						})
					
						break;

				case "accept":
					
					ask("Provide phone: ")
						.then(phone => {
							
							client.emit('challenge_accepted', fake_id, phone);
							sendCmd();
						})
					
						break;
					
				case "create":
					
					ask("Provide phone: ")
						.then(phone => {
							
							client.emit('hello', UUID.generate(), fake_lang, phone);
							sendCmd();
						})
					
					break;

				case "char":

					if (secret) {

						ask("Provide purpose: ")
							.then(char => {
								
								client.emit('tried_char', fake_id, char, secret.includes(char) ? "success" : "failed");
								sendCmd();
							})
						 
						break;
						
					} else {

						console.log("Please issue a \"pair\" command first.");
						sendCmd();
					}
					break;
					
				case "finish": 

					if (secret) {
						
						client.emit('match_finished', fake_id); 
						secret = null;
						break;
						
					} else {

						console.log("Please issue a \"pair\" command first.");
					}

					sendCmd();
					
					break;

				default:
					sendCmd();
					break;

			}


		})
	
	
	
}

client.on('connect', () => {
	
	client.emit('hello', fake_id, fake_lang, fake_phone);

	sendCmd();

});

client.on('broadcast', message => {

	if (message.player1.id == fake_id)
		secret = message.player2.secret
	else if (message.player2.id == fake_id)
		secret = message.player1.secret

});

	
	/* 
	   client.on('error', console.error);
	   client.on('connect_error', console.error);
	   client.on('connect_timeout', console.error);
	   client.on('disconnect', console.error);

	   client.on('reconnect', console.error);
	   client.on('reconnect_attempt', console.error);
	   client.on('reconnecting', console.error);
	   client.on('reconnect_error', console.error);
	   client.on('reconnect_failed', console.error); */
