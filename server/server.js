

let async = require('async');
let mongoose = require('mongoose');
let schemas = require('./schemas');
let Player = schemas.player;
let Match = schemas.match;

// note, io(<port>) will create a http server for you
let io = require('socket.io')(80);


mongoose.connection.on('error', console.error);
mongoose.connection.once('open',

	() => {

		io.on('connection', (socket) => {

			console.log("Connected: " + socket.id); /* Si no hay info del socket (que no habrá) request hello?? (que deberia venir)*/

			socket.emit("main", {action: "who_are_you"});
			
			socket.on('hello', (id, lang, phone) => {

				console.log("New player: " + id + " lang: " + lang);

				Player.findById(id)
					.then(
						player => {

							if (player) {

								player.lang = lang;
								player.phone = phone;
								player.socket = socket.id;
								player.secret = null;
								player.pairing = false;
								player.paired = false;
								player.secret = null;
								player.checked = 0;

								
							} else {
								
								player = new Player({
								
									id: id,
									lang: lang,
									phone: phone,
									socket: socket.id,
									secret: null
									
								})
							}

							player.save((err) => {
							
								if (err)
									console.error(err);
								
							})
								
						}
					)
			});

			socket.on('new-lang', (id, lang) => {

				console.log("New lang for: " + id + " ===> " + lang);
				
				Player.findById(id)
					.then(player => {
						
						player.lang = lang;
						player.save((err) => {
							
							if (err)
								console.error(err);
						})
							
					}, console.error)
				
			});

			socket.on('new-phone', (id, phone) => {

				console.log("New phone for: " + id + " ===> " + phone);
				
				Player.findById(id)
					.then(player => {
						
						player.phone = phone;
						player.save((err) => {
							
							if (err)
								console.error(err);
						})
							
					}, console.error)
				
			});

			socket.on("challenge_rejected", (id, phone, noti_id) => {

				io.emit("main", {action: "clean_noti", id: id, noti_id: noti_id });
				
				Player.findByPhone(phone)
					.then(player => {
						
						io.emit('challenge', { action: "reject", id: player.id });
						
					});
				
			})

			socket.on('challenge', (id, phone, arr) => {

				console.log("Challenge: " + id + " ===> " + arr[0]);
				
				async.each(arr,
					(phone, next) => {
						
						Player.findByPhone(phone)
							.then(player => {
								
								if (player) 
									next(player);
								else
									next();
								
							});
						
					}, player => {

						if (player) {
							
							io.emit("main", {
								action: "notify",
								phone: phone,
								id: player.id
							})
						}
						
					});
			});

			socket.on('challenge_accepted', (id, phone, noti_id) => {
				
				console.log("challenge_accepted: " + id + " ===> " + phone);

				io.emit("main", {action: "clean_noti", id: id, noti_id: noti_id });
				
				Player.findByPhone(phone)
					.then(player => {

						new Match({
							
							player1: id,
							player2: player.id,
							challenge: true
							
						}).save(err => {
							
							if (err)
								console.error(err);
							
							io.emit('challenge', { action: "accept", id: player.id });

						})
							
					});
			});

			socket.on('challenge_pairing', (id, word) => {
						
				Player.findById(id)
					.then(player => {

						player.secret = word

						player.save(err => {
							
							if (err)
								console.error(err);

							Match.setReady(id)
								.then(err => {

									if (err)
										console.error(err);

									Match.isReady(id)
										.then(res => {

											if (res) {

												Match.findOpponent(id)
													.then(opponent_id => {

														Player.findById(opponent_id)
															.then(opponent => {

																let message = {

																	action: "paired",
																	player1: { id: player.id, secret: player.secret },
																	player2: { id: opponent.id, secret: opponent.secret }
																	
																};

																io.emit('broadcast', message);
																
															})
															
													})
											}

										})
								})
						});
						
						
						
					});
			});
			
			socket.on('is_online', (id, phones, cb) => {

				Player.findById(id)
					.then(challenger => {
						
						async.each(phones,
							(phone, next) => {

								Player.findByPhone(phone)
									.then(player => {

										if (player) 
											next(player);
										else
											next();
										
									});
								
							}, player => {
								
								cb(player && player.id != id && player.lang == challenger.lang ? true : false);
								
							});
					});
				
			});

			
			socket.on('match_pairing', (id, secret) => {

				console.log("New waiter: " + id + " ===> " + secret);
				
				Player.findById(id)
					.then(player => {
					
						player.secret = secret;
						player.pairing = true;
						
						player.save(err => {
							
							if (err)
								console.error(err);
						})
							
					}, console.error);
			});

			socket.on('wait_no_more', (id) => {

				console.log("Bye waiter: " + id);
				
				Player.findById(id)
					.then(player => {
					
						player.secret = null;
						player.pairing = false;
						player.paired = false;
						
						player.save(err => {
							
							if (err)
								console.error(err);
						})
							
					}, console.error);
			});

			socket.on('tried_char', (id, string, action) => {
				
				Match.findOpponent(id)
					.then(opponent => {

						io.emit("match",
							{
								action: action,
								id: opponent,
								word: string
							}
						);
						
					}, console.error)
				
			});

			socket.on('match_finished', (id) => {

				/* Ideal, no ties */
				
				Match.findOpponent(id)
					.then(opponent => {
						
						io.emit("match",
							{
								action: "finished",
								id: opponent
							}
						);

						Match.deleteMatch(id, opponent)
							.then(match => {

								async.each([match.player1, match.player2],
									(id, next) => {

										Player.findById(id)
											.then(
												player => {
													
													player.pairing = false;
													player.paired = false;
													player.secret = null;

													player.save(err => {

														if (err)
															console.error(err);

														next();

													})
												}
											)
									}
								)
								
							})
						
					}, console.error)
				
			});
			
						
			socket.on('disconnect', function () {

				Player.deletePlayer(socket.id)
					.then(player => {

						console.log("Player " + player.id + " deleted.");
							
					}, console.error)
				
			});
		});


		setInterval(() => {

			Player.findWaiters()
				.then(waiters => {
					
					for (var i = 0; i < waiters.length; i++) {

						let player1 = waiters[i];
						
						if (player1.paired)
							continue;

						let j = i + 1;
						
						while (j < waiters.length) {

							if (waiters[j].paired || waiters[j].lang != player1.lang)
								j++;
							else
								break;

						}

						if (j < waiters.length) {

							let player2 = waiters[j];

							let message = {

								action: "paired",
								player1: { id: player1.id, secret: player1.secret },
								player2: { id: player2.id, secret: player2.secret }

							};
							
							player1.paired = true;
							player2.paired = true;

							player1.save(err => {

								if (err)
									console.error(err);
								
								player2.save(err => {
									
									if (err)
										console.error(err);

									new Match({
										
										player1: player1.id,
										player2: player2.id

									}).save(err => {

										if (err)
											console.error(err);
										
										io.emit('broadcast', message);
									})
										
										
								})
									
									
							})
						}
					}
						
			}, console.error)
				
				
		}, 500)
			
	}
	
)

mongoose.connect('mongodb://localhost/test');


